package com.example.owner_pc.textureview;

import android.app.Activity;
import android.content.Context;
import android.content.SyncRequest;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.TextureView;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import java.io.IOException;
import android.view.TextureView.SurfaceTextureListener;
import android.annotation.SuppressLint;
import android.graphics.SurfaceTexture;
import android.widget.FrameLayout;
import android.widget.Toast;


public class MainActivity extends Activity implements SurfaceTextureListener {
    private TextureView myTexture;
    private Camera mCamera;


    @SuppressLint("NewApi")


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myTexture = new TextureView(this);
        myTexture.setSurfaceTextureListener(this);
        setContentView(myTexture);
        checkCameraHardware(getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @SuppressLint("NewApi")
    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture arg0, int arg1, int arg2){
        mCamera = Camera.open();
        Camera.Size previewSize = mCamera.getParameters().getPreviewSize();
        myTexture.setLayoutParams(new FrameLayout.LayoutParams(previewSize.width, previewSize.height, Gravity.CENTER));
        try{
            mCamera.setPreviewTexture(arg0);
        }catch (IOException t){

        }
        mCamera.startPreview();
        myTexture.setAlpha(1.0f);
        myTexture.setRotation(90.0f);
    }
    private boolean checkCameraHardware(Context context){
        if(context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            Toast.makeText(context,"Camera available",Toast.LENGTH_LONG).show();
            return true;


        }else{
            return false;
        }

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture arg0){
        mCamera.stopPreview();
        mCamera.release();
        return true;
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture arg0, int arg1, int arg2){

    }
    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture arg0){

    }

}
